
provider "aws" {
  region = var.region
}
module "vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  name                 = "squid-vpc"
  cidr                 = "12.0.0.0/16"
  azs                  = var.az
  public_subnets       = var.public_subnets
  enable_nat_gateway   = false
  enable_dns_hostnames = true
}

### Security group
module "squidproxy-security-group" {
  source              = "terraform-aws-modules/security-group/aws"
  name                = "squidproxy-sg"
  description         = "security group for squid proxy"
  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = []
  ingress_rules       = []
  egress_rules        = ["all-all"]
  ingress_with_cidr_blocks = [
    {
      rule        = "squid-proxy-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}
### Network load balancer
module "nlb" {
  source             = "terraform-aws-modules/alb/aws"
  name               = "squid-nlb"
  load_balancer_type = "network"
  vpc_id             = module.vpc.vpc_id
  subnets            = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  target_groups = [
    {
      name             = "sqtg"
      backend_protocol = "TCP"
      backend_port     = 3128
      target_type      = "instance"
    }
  ]
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]
  tags = {
    Environment = "Test"
  }
}
### Auto-scaling group
module "asg" {
  source = "terraform-aws-modules/autoscaling/aws"
  # Autoscaling group
  name                      = "squid-asg"
  min_size                  = 1
  max_size                  = 2
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  target_group_arns         = module.nlb.target_group_arns
  # Launch template
  lt_name                = "squid-template"
  description            = "squid proxy template "
  update_default_version = true
  use_lt                 = true
  create_lt              = true
  image_id               = "ami-00399ec92321828f5" #ami-00399ec92321828f5
  instance_type          = "t2.micro"
  ebs_optimized          = false
  enable_monitoring      = true
  key_name               = "ansiblekey"
  security_groups        = [module.squidproxy-security-group.security_group_id]
  tag_specifications = [
    {
      resource_type = "instance"
      tags = {
        Name = "squid-instance"
      }
    }
  ]

}