
variable "region" {
  type        = string
  default     = "us-east-2"
  description = "Enter the region in which Infra will created"
}
variable "az" {
  type        = list(string)
  default     = ["us-east-2a", "us-east-2b", "us-east-2c"]
  description = "Enter the Availability Zones"
}
#variable "private_subnets" {
#  type = list(string)
#  default = ["12.0.1.0/24", "12.0.2.0/24", "12.0.3.0/24"]
#  description = "Enter the private subnets cidr"
#}

variable "public_subnets" {
  type        = list(string)
  default     = ["12.0.101.0/24", "12.0.102.0/24"]
  description = "Enter the public subnet cidr"
}
